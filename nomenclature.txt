*** NOMENCLATURE ***

Les constantes : JE_SUIS_UNE_CONSTANTE
Les fonctions : jeSuisUneFonction
Les attributes de classe : jeSuisUnAttribut
Boolean : isJeSuisUnBoolean
Les classes : JeSuisUneClasse
Les Enum : EJeSuisUnEnum
Les Attributs d'enum : JESUISUNATTRIBUT
Les tableaux : aux pluriels
Les variables récupérées du HTML : typebloc_je_suis_un_bloc (ex : div_mon_bloc)